# CareSyntax 

### CHALLENGE

This project is made essainsally using a Microservice approche

  - Spring boot , spring JPA , Hibernate 
  - Vuejs , Quasar FrameWork 
  - IntelliJ

### Features

  - Manage Patiens and Study cases
  - Scheduling procedures
  - Updating status of procedure


You have also:
  - Server Side pagging 
  - One page website approch
  - Project is cross-platform : Web , Mobile , Desktop (Check Quasar FrameWork for more information)

### Installation

Installation for front end :

```sh
$ npm install
$ npm install -g vue-cli
$ npm install -g quasar-cli
$ quasar dev 
```

For back end ...

```sh
$ mvn clean install
$ Run project 
```