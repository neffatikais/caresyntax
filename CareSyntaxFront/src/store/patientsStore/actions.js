import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

export const getAllPatients = ({state}) => {
  return new Promise((resolve, reject) => {
    Vue.http.post(process.env.backend.URLPatients + 'search/', state.searchRequest).then((res) => {
      resolve(res)
    })
  })
}
export const addPatient = ({state}) => {
  return new Promise((resolve, reject) => {
    Vue.http.post(process.env.backend.URLPatients + 'createOrUpdate/', state.body).then((res) => {
      resolve(res)
    })
  })
}
export const getPatient = ({state}) => {
  return new Promise((resolve, reject) => {
    Vue.http.get(process.env.backend.URLPatients + state.id).then((res) => {
      resolve(res)
    })
  })
}
export const deletePatient = ({state}) => {
  return new Promise((resolve, reject) => {
    Vue.http.get(process.env.backend.URLPatients + 'delete/' + state.id).then((res) => {
      resolve(res)
    })
  })
}
