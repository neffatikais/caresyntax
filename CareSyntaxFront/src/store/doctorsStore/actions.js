import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

export const getAllDoctors = ({state}) => {
  return new Promise((resolve, reject) => {
    Vue.http.post(process.env.backend.URLDoctors, state.searchRequest).then((res) => {
      resolve(res)
    })
  })
}
export const getAllDoctorsByFirstName = ({state}) => {
  return new Promise((resolve, reject) => {
    Vue.http.post(process.env.backend.URLDoctors + 'search/', state.searchRequest).then((res) => {
      resolve(res)
    })
  })
}
