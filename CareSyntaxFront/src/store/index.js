import Vue from 'vue'
import Vuex from 'vuex'

import patientsStore from './patientsStore'
import doctorsStore from './doctorsStore'
import roomsStore from './roomsStore'
import studiesStore from './studiesStore'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    patientsStore,
    doctorsStore,
    roomsStore,
    studiesStore
  }
})

export default store
