import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

export const getAllStudies = ({state}) => {
  return new Promise((resolve, reject) => {
    Vue.http.post(process.env.backend.URLStudies + 'search/', state.searchRequest).then((res) => {
      resolve(res)
    })
  })
}

export const addStudy = ({state}) => {
  return new Promise((resolve, reject) => {
    Vue.http.post(process.env.backend.URLStudies + 'createOrUpdate/', state.body).then((res) => {
      resolve(res)
    })
  })
}

export const getStudy = ({state}) => {
  return new Promise((resolve, reject) => {
    Vue.http.get(process.env.backend.URLStudies + state.id).then((res) => {
      resolve(res)
    })
  })
}

export const deleteStudy = ({state}) => {
  return new Promise((resolve, reject) => {
    Vue.http.get(process.env.backend.URLStudies + 'delete/' + state.id).then((res) => {
      resolve(res)
    })
  })
}
