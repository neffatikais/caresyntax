export default {
  body: {},
  id: '',
  searchRequest: {
    currentPageIndex: 0,
    pageMaxIndex: 5,
    filter: '',
    criteria: '',
    value: '',
    direction: 'DESC'
  }
}
