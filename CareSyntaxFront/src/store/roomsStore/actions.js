import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

export const getAllRooms = ({state}) => {
  return new Promise((resolve, reject) => {
    Vue.http.post(process.env.backend.URLRooms, state.searchRequest).then((res) => {
      resolve(res)
    })
  })
}

export const getAllRoomsByName = ({state}) => {
  return new Promise((resolve, reject) => {
    Vue.http.post(process.env.backend.URLRooms + 'search/', state.searchRequest).then((res) => {
      resolve(res)
    })
  })
}
