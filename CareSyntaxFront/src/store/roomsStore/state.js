export default {
  body: {},
  searchRequest: {
    currentPageIndex: 0,
    pageMaxIndex: 5,
    filter: '',
    criteria: '',
    value: '',
    direction: 'DESC'
  }
}
