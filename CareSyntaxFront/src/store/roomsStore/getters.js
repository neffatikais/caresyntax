export const getBody = (state) => {
  return state.body
}
export const getSearchRequest = (state) => {
  return state.searchRequest
}
