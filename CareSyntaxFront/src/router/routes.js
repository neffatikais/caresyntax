export default [
  {
    path: '/',
    component: () => import('layouts/default'),
    children: [
      {path: '', component: () => import('pages/index')},
      {
        path: '/patients',
        component: () => import('layouts/tabLayout'),
        children: [
          {path: '', component: () => import('pages/patients')},
          {path: 'add', component: () => import('pages/addPatient')},
          {path: 'view/:idStudy', name: 'viewStudy', component: () => import('pages/editStudy')},
          {path: 'view/:idStudy', name: 'addStudy', component: () => import('pages/addStudy')},
          {
            path: 'edit/:id',
            name: 'editPatient',
            component: () => import('pages/editPatient')
          }
        ]
      },
      {
        path: '/rooms',
        component: () => import('layouts/tabLayout'),
        children: [
          {path: '', component: () => import('pages/rooms')},
          {path: 'view/:id', name: 'viewRoom', component: () => import('pages/addPatient')}
        ]
      },
      {
        path: '/doctors',
        component: () => import('layouts/tabLayout'),
        children: [
          {path: '', component: () => import('pages/doctors')},
          {path: 'view/:id', name: 'viewDoctor', component: () => import('pages/addPatient')}
        ]
      }
    ]
  },

  { // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]
