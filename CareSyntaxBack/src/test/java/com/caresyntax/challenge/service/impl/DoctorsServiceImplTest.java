package com.caresyntax.challenge.service.impl;

import com.caresyntax.challenge.dataaccessobject.DoctorsRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;

import static com.caresyntax.challenge.utils.TestUtils.InitEntities.initSearchRequest;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;


@RunWith(PowerMockRunner.class)
@PrepareForTest(PageRequest.class)
public class DoctorsServiceImplTest {

    @Mock
    private DoctorsRepository doctorsRepository;

    @InjectMocks
    private DoctorsServiceImpl doctorsService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockPageRequest();
        mockDoctorsRepository();
    }

    private void mockPageRequest() {
        PowerMockito.mockStatic(PageRequest.class);
        BDDMockito.given(PageRequest.of(any(Integer.class), any(Integer.class))).willReturn(new PageRequest(1, 5));
    }

    private void mockDoctorsRepository() {
        Mockito.when(doctorsRepository.findAll(any(Pageable.class))).thenReturn(new PageImpl<>(new ArrayList<>()));
        Mockito.when(doctorsRepository.findAllByFirstNameContaining(anyString(), any(Pageable.class))).thenReturn(new ArrayList<>());
        Mockito.when(doctorsRepository.findAllByFirstNameNotContaining(anyString(), any(Pageable.class))).thenReturn(new ArrayList<>());
        Mockito.when(doctorsRepository.findAllByLastNameContaining(anyString(), any(Pageable.class))).thenReturn(new ArrayList<>());
        Mockito.when(doctorsRepository.findAllByLastNameNotContaining(anyString(), any(Pageable.class))).thenReturn(new ArrayList<>());
    }

    @Test
    public void findAll() throws Exception {
        doctorsService.findAll(initSearchRequest());
    }

    @Test
    public void search() throws Exception {
        doctorsService.search(initSearchRequest());
    }
}