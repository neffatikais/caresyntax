/**
 * CREATE Script for init of DB
 */

-- Create 10 patient

INSERT INTO PATIENT (id, first_name, last_name, sex, birth_date, created_date, deleted)
VALUES ('10000', 'patientFirstName', 'patientLastName', 'MALE', now(), now(), FALSE);

INSERT INTO PATIENT (id, first_name, last_name, sex, birth_date, created_date, deleted)
VALUES ('10001', 'patient1FirstName', 'patient1LastName', 'MALE', now(), now(), FALSE);

INSERT INTO PATIENT (id, first_name, last_name, sex, birth_date, created_date, deleted)
VALUES ('10002', 'patient2FirstName', 'patient2LastName', 'FEMALE', now(), now(), FALSE);

INSERT INTO PATIENT (id, first_name, last_name, sex, birth_date, created_date, deleted)
VALUES ('10003', 'patient3FirstName', 'patient3LastName', 'MALE', now(), now(), FALSE);

INSERT INTO PATIENT (id, first_name, last_name, sex, birth_date, created_date, deleted)
VALUES ('10004', 'patient4FirstName', 'patient4LastName', 'FEMALE', now(), now(), FALSE);

INSERT INTO PATIENT (id, first_name, last_name, sex, birth_date, created_date, deleted)
VALUES ('10005', 'patient5FirstName', 'patient5LastName', 'MALE', now(), now(), FALSE);

INSERT INTO PATIENT (id, first_name, last_name, sex, birth_date, created_date, deleted)
VALUES ('10006', 'patient6FirstName', 'patient6LastName', 'FEMALE', now(), now(), FALSE);

INSERT INTO PATIENT (id, first_name, last_name, sex, birth_date, created_date, deleted)
VALUES ('10007', 'patient7FirstName', 'patient7LastName', 'MALE', now(), now(), FALSE);

INSERT INTO PATIENT (id, first_name, last_name, sex, birth_date, created_date, deleted)
VALUES ('10008', 'patient8FirstName', 'patient8LastName', 'FEMALE', now(), now(), FALSE);

INSERT INTO PATIENT (id, first_name, last_name, sex, birth_date, created_date, deleted)
VALUES ('10009', 'patient9FirstName', 'patient9LastName', 'MALE', now(), now(), FALSE);

-- Create 10 doctors

INSERT INTO DOCTOR (id, first_name, last_name, speciality, created_date, deleted)
VALUES ('20000', 'doctorFirstName', 'doctorSecondName', 'Something', now(), FALSE);

INSERT INTO DOCTOR (id, first_name, last_name, speciality, created_date, deleted)
VALUES ('20001', 'doctor1FirstName', 'doctorSecondName', 'Something', now(), FALSE);

INSERT INTO DOCTOR (id, first_name, last_name, speciality, created_date, deleted)
VALUES ('20002', 'doctor2FirstName', 'doctorSecondName', 'Something', now(), FALSE);

INSERT INTO DOCTOR (id, first_name, last_name, speciality, created_date, deleted)
VALUES ('20003', 'doctor3FirstName', 'doctorSecondName', 'Something', now(), FALSE);

INSERT INTO DOCTOR (id, first_name, last_name, speciality, created_date, deleted)
VALUES ('20004', 'doctor4FirstName', 'doctorSecondName', 'Something', now(), FALSE);

INSERT INTO DOCTOR (id, first_name, last_name, speciality, created_date, deleted)
VALUES ('20005', 'doctor5FirstName', 'doctorSecondName', 'Something', now(), FALSE);

INSERT INTO DOCTOR (id, first_name, last_name, speciality, created_date, deleted)
VALUES ('20006', 'doctor6FirstName', 'doctorSecondName', 'Something', now(), FALSE);

INSERT INTO DOCTOR (id, first_name, last_name, speciality, created_date, deleted)
VALUES ('20007', 'doctor7FirstName', 'doctorSecondName', 'Something', now(), FALSE);

INSERT INTO DOCTOR (id, first_name, last_name, speciality, created_date, deleted)
VALUES ('20008', 'doctor8FirstName', 'doctorSecondName', 'Something', now(), FALSE);

INSERT INTO DOCTOR (id, first_name, last_name, speciality, created_date, deleted)
VALUES ('20009', 'doctor9FirstName', 'doctorSecondName', 'Something', now(), FALSE);

-- Create 10 rooms

INSERT INTO ROOM (id, name, capacity, created_date, deleted) VALUES ('30000', 'roomName', 1, now(), FALSE);

INSERT INTO ROOM (id, name, capacity, created_date, deleted) VALUES ('30001', 'room1Name', 2, now(), FALSE);

INSERT INTO ROOM (id, name, capacity, created_date, deleted) VALUES ('30002', 'room2Name', 3, now(), FALSE);

INSERT INTO ROOM (id, name, capacity, created_date, deleted) VALUES ('30003', 'room3Name', 1, now(), FALSE);

INSERT INTO ROOM (id, name, capacity, created_date, deleted) VALUES ('30004', 'room4Name', 2, now(), FALSE);

INSERT INTO ROOM (id, name, capacity, created_date, deleted) VALUES ('30005', 'room5Name', 3, now(), FALSE);

INSERT INTO ROOM (id, name, capacity, created_date, deleted) VALUES ('30006', 'room6Name', 1, now(), FALSE);

INSERT INTO ROOM (id, name, capacity, created_date, deleted) VALUES ('30007', 'room7Name', 2, now(), FALSE);

INSERT INTO ROOM (id, name, capacity, created_date, deleted) VALUES ('30008', 'room8Name', 3, now(), FALSE);

INSERT INTO ROOM (id, name, capacity, created_date, deleted) VALUES ('30009', 'room9Name', 1, now(), FALSE);

-- Create 10 Study