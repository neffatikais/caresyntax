package com.caresyntax.challenge.utils.mapperUtils.impl;

import com.caresyntax.challenge.datatransferobject.impl.DoctorDTO;
import com.caresyntax.challenge.domainObjects.model.Doctor;
import com.caresyntax.challenge.utils.mapperUtils.Mapper;
import org.springframework.stereotype.Component;

import static com.caresyntax.challenge.datatransferobject.impl.DoctorDTO.DoctorDTOBuilder;
import static com.caresyntax.challenge.datatransferobject.impl.DoctorDTO.newBuilder;

@Component
public class DoctorMapper implements Mapper<Doctor, DoctorDTO> {
    @Override
    public Doctor makeDO(DoctorDTO dTO) {
        return new Doctor(dTO.getId(), dTO.getFirstName(), dTO.getLastName(), dTO.getSpeciality());

    }

    @Override
    public DoctorDTO makeDTO(Doctor dO) {
        DoctorDTOBuilder doctorDTOBuilder = newBuilder().
                setId(dO.getId()).
                setFirstName(dO.getFirstName()).
                setLastName(dO.getLastName()).
                setSpeciality(dO.getSpeciality());
        return doctorDTOBuilder.createDoctorDTO();
    }

}