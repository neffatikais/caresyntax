package com.caresyntax.challenge.utils.TestUtils;

import com.caresyntax.challenge.domainObjects.request.SearchRequest;

/**
 * Utils for tests : provides init for all entities
 */
public class InitEntities {
    public static SearchRequest initSearchRequest() {
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.setCurrentPageIndex(1);
        searchRequest.setPageMaxIndex(5);
        searchRequest.setCriteria("firstName");
        searchRequest.setOperand("=");
        return searchRequest;
    }
}
