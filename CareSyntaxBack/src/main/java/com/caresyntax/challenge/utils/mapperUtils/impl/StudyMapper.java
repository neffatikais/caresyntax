package com.caresyntax.challenge.utils.mapperUtils.impl;

import com.caresyntax.challenge.datatransferobject.impl.StudyDTO;
import com.caresyntax.challenge.datatransferobject.impl.StudyDTO.StudyDTOBuilder;
import com.caresyntax.challenge.domainObjects.model.Study;
import com.caresyntax.challenge.utils.mapperUtils.Mapper;
import org.springframework.stereotype.Component;

import static com.caresyntax.challenge.datatransferobject.impl.StudyDTO.newBuilder;

@Component
public class StudyMapper implements Mapper<Study, StudyDTO> {

    @Override
    public Study makeDO(StudyDTO dTO) {
        return new Study(dTO.getId(), dTO.getPatient(), dTO.getDoctorList(), dTO.getRoom(), dTO.getDescription(), dTO.getStudyStatus(), dTO.getStartTime(), dTO.getEndTime());
    }

    @Override
    public StudyDTO makeDTO(Study dO) {
        StudyDTOBuilder studyDTOBuilder = newBuilder().
                setId(dO.getId()).
                setStudyStatus(dO.getStudyStatus()).
                setDescription(dO.getDescription()).
                setDoctorList(dO.getDoctorList()).
                setEndTime(dO.getEndTime()).
                setPatient(dO.getPatient()).
                setRoom(dO.getRoom()).
                setStartTime(dO.getStartTime());
        return studyDTOBuilder.createStudyDTO();
    }

}
