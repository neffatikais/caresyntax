package com.caresyntax.challenge.utils.mapperUtils;

import com.caresyntax.challenge.datatransferobject.DataTransferObject;
import com.caresyntax.challenge.domainObjects.DomainObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public interface Mapper<T extends DomainObject, D extends DataTransferObject> {

    /**
     * @param dTO data transfer object
     * @return domain object
     */
    T makeDO(D dTO);

    /**
     * @param dO domain object
     * @return data transfer object
     */
    D makeDTO(T dO);


    /**
     * @param dOList domain object list
     * @return data transfer object
     */
    default List<D> makeDTOList(final Collection<T> dOList) {
        return dOList.stream()
                .map(this::makeDTO)
                .collect(Collectors.toList());
    }

    /**
     * @param dTOList data transfer object list
     * @return domain object list
     */
    default List<T> makeDOList(final Collection<D> dTOList) {
        return dTOList.stream()
                .map(this::makeDO)
                .collect(Collectors.toList());
    }

    /**
     * @param dOPage domain object page
     * @return data transfer object page
     */
    default Page<D> makeDTOPage(final Page<T> dOPage) {
        List<D> collect = dOPage.getContent().stream()
                .map(this::makeDTO)
                .collect(Collectors.toList());
        return new PageImpl<D>(collect, dOPage.getPageable(), dOPage.getTotalElements());
    }

}