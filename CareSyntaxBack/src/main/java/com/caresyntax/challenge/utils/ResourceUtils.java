package com.caresyntax.challenge.utils;

import java.util.UUID;

public class ResourceUtils {

    /**
     * Generate a new, random hex-encoded globally unique id.
     *
     * @return base64 encoded UTF8 string
     */
    public static String generateUniqueId() {
        // A UUID represents a 128-bit value, that is 16 bytes long value.
        final UUID uuid = UUID.randomUUID();
        return String.valueOf(Math.abs(uuid.getMostSignificantBits()));
    }
}
