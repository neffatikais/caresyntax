package com.caresyntax.challenge.utils.mapperUtils.impl;

import com.caresyntax.challenge.datatransferobject.impl.RoomDTO;
import com.caresyntax.challenge.domainObjects.model.Room;
import com.caresyntax.challenge.utils.mapperUtils.Mapper;
import org.springframework.stereotype.Component;

import static com.caresyntax.challenge.datatransferobject.impl.RoomDTO.RoomDTOBuilder;
import static com.caresyntax.challenge.datatransferobject.impl.RoomDTO.newBuilder;

@Component
public class RoomMapper implements Mapper<Room, RoomDTO> {
    @Override
    public Room makeDO(RoomDTO dTO) {
        return new Room(dTO.getId(), dTO.getName(), dTO.getCapacity());

    }

    @Override
    public RoomDTO makeDTO(Room dO) {
        RoomDTOBuilder roomDTOBuilder = newBuilder().
                setId(dO.getId()).
                setName(dO.getName()).
                setCapacity(dO.getCapacity());
        return roomDTOBuilder.createRoomDTO();
    }

}
