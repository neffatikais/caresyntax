package com.caresyntax.challenge.utils.mapperUtils.impl;

import com.caresyntax.challenge.datatransferobject.impl.PatientDTO;
import com.caresyntax.challenge.domainObjects.model.Patient;
import com.caresyntax.challenge.utils.mapperUtils.Mapper;
import org.springframework.stereotype.Component;

import static com.caresyntax.challenge.datatransferobject.impl.PatientDTO.newBuilder;

@Component
public class PatientMapper implements Mapper<Patient, PatientDTO> {
    @Override
    public Patient makeDO(PatientDTO dTO) {
        return new Patient(dTO.getId(), dTO.getFirstName(), dTO.getLastName(), dTO.getSex(), dTO.getBirthDate(), dTO.getStudyList());
    }

    @Override
    public PatientDTO makeDTO(Patient dO) {
        PatientDTO.PatientDTOBuilder patientDTOBuilder = newBuilder().
                setId(dO.getId()).
                setFirstName(dO.getFirstName()).
                setLastName(dO.getLastName()).
                setSex(dO.getSex()).
                setBirthDate(dO.getBirthDate()).
                setStudyList(dO.getStudyList());
        return patientDTOBuilder.createPatientDTO();
    }

}
