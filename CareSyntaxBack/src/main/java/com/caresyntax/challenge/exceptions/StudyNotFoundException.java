package com.caresyntax.challenge.exceptions;

/**
 * Study Not found Exception
 */
public class StudyNotFoundException extends Exception {

    public StudyNotFoundException(String message) {
        super(message);
    }

}
