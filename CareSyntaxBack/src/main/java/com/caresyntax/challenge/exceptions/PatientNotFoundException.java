package com.caresyntax.challenge.exceptions;

/**
 * Patient not found Exception
 */
public class PatientNotFoundException extends Exception {

    public PatientNotFoundException(String message) {
        super(message);
    }

}
