package com.caresyntax.challenge.service;

import com.caresyntax.challenge.domainObjects.model.Study;
import com.caresyntax.challenge.domainObjects.request.SearchRequest;
import com.caresyntax.challenge.exceptions.StudyNotFoundException;
import org.springframework.data.domain.Page;

public interface StudiesService {
    Study findById(String id) throws StudyNotFoundException;

    Page<Study> search(SearchRequest request);

    void saveOrUpdate(Study study);

    Page<Study> findAll(SearchRequest request);

    void delete(String id);
}
