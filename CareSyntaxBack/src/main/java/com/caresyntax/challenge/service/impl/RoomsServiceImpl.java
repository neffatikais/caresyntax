package com.caresyntax.challenge.service.impl;

import com.caresyntax.challenge.dataaccessobject.RoomsRepository;
import com.caresyntax.challenge.domainObjects.model.Room;
import com.caresyntax.challenge.domainObjects.request.SearchRequest;
import com.caresyntax.challenge.service.RoomsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RoomsServiceImpl implements RoomsService {

    private final RoomsRepository roomsRepository;

    @Autowired
    public RoomsServiceImpl(RoomsRepository roomsRepository) {
        this.roomsRepository = roomsRepository;
    }

    @Override
    public Page<Room> findAll(SearchRequest request) {
        PageRequest pageRequest = PageRequest.of(request.getCurrentPageIndex(), request.getPageMaxIndex());
        return roomsRepository.findAll(pageRequest);
    }

    @Override
    public Page<Room> search(SearchRequest request) {
        String operand = request.getOperand();
        String value = request.getValue();
        List<Room> roomList = new ArrayList<>();
        PageRequest pageRequest = PageRequest.of(request.getCurrentPageIndex(), request.getPageMaxIndex());
        switch (request.getCriteria()) {
            case "name":
                if (operand.equals("=")) {
                    roomList = roomsRepository.findAllByNameContaining(value, pageRequest);
                } else if (operand.equals("!=")) {
                    roomList = roomsRepository.findAllByNameNotContaining(value, pageRequest);
                }
                break;
            default:
                break;
        }
        return new PageImpl<>(roomList);
    }
}
