package com.caresyntax.challenge.service;

import com.caresyntax.challenge.domainObjects.model.Patient;
import com.caresyntax.challenge.domainObjects.request.SearchRequest;
import com.caresyntax.challenge.exceptions.PatientNotFoundException;
import org.springframework.data.domain.Page;

public interface PatientsService {
    Page<Patient> search(SearchRequest request);

    void create(Patient request);

    Patient findById(String id) throws PatientNotFoundException;

    Page<Patient> findAll(SearchRequest request);

    void delete(String id);
}
