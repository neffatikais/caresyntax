package com.caresyntax.challenge.service;

import com.caresyntax.challenge.domainObjects.model.Room;
import com.caresyntax.challenge.domainObjects.request.SearchRequest;
import org.springframework.data.domain.Page;

public interface RoomsService {
    Page<Room> findAll(SearchRequest request);

    Page<Room> search(SearchRequest request);
}
