package com.caresyntax.challenge.service.impl;

import com.caresyntax.challenge.dataaccessobject.StudiesRepository;
import com.caresyntax.challenge.domainObjects.model.Study;
import com.caresyntax.challenge.domainObjects.request.SearchRequest;
import com.caresyntax.challenge.exceptions.StudyNotFoundException;
import com.caresyntax.challenge.service.StudiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;

@Service
public class StudiesServiceImpl implements StudiesService {

    private final StudiesRepository studiesRepository;

    @Autowired
    public StudiesServiceImpl(StudiesRepository studiesRepository) {
        this.studiesRepository = studiesRepository;
    }

    @Override
    public Study findById(String id) throws StudyNotFoundException {
        Study study = studiesRepository.findById(id).orElse(null);
        if (study == null) {
            throw new StudyNotFoundException("Study not found !");
        }
        return study;
    }

    @Override
    public Page<Study> search(SearchRequest request) {
        String value = request.getValue();
        String filter = request.getFilter();
        Page<Study> studyList;
        PageRequest pageRequest = PageRequest.of(request.getCurrentPageIndex(), request.getPageMaxIndex());
        switch (request.getCriteria()) {
            case "patient":
                try {
                    studyList = studiesRepository.findAllByPatientIdAndDeletedIsFalseAndStartTimeContaining(value, ZonedDateTime.parse(filter), pageRequest);
                } catch (DateTimeParseException ex) {
                    studyList = studiesRepository.findAllByPatientIdAndDeletedIsFalse(value, pageRequest);
                }
                break;
            default:
                studyList = studiesRepository.findAllByDeletedIsFalse(pageRequest);
                break;
        }
        return studyList;
    }

    @Override
    public void saveOrUpdate(Study study) {
        studiesRepository.save(study);
    }

    @Override
    public Page<Study> findAll(SearchRequest request) {
        PageRequest pageRequest = PageRequest.of(request.getCurrentPageIndex(), request.getPageMaxIndex());
        return studiesRepository.findAllByDeletedIsFalse(pageRequest);
    }

    @Override
    public void delete(String id) {
        try {
            Study study = findById(id);
            study.setDeleted(true);
            studiesRepository.save(study);
        } catch (StudyNotFoundException e) {
            e.printStackTrace();
        }
    }
}
