package com.caresyntax.challenge.service;

import com.caresyntax.challenge.domainObjects.model.Doctor;
import com.caresyntax.challenge.domainObjects.request.SearchRequest;
import org.springframework.data.domain.Page;

public interface DoctorsService {
    Page<Doctor> findAll(SearchRequest request);

    Page<Doctor> search(SearchRequest request);
}
