package com.caresyntax.challenge.service.impl;

import com.caresyntax.challenge.dataaccessobject.PatientsRepository;
import com.caresyntax.challenge.domainObjects.model.Patient;
import com.caresyntax.challenge.domainObjects.request.SearchRequest;
import com.caresyntax.challenge.exceptions.PatientNotFoundException;
import com.caresyntax.challenge.service.PatientsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class PatientsServiceImpl implements PatientsService {

    private final PatientsRepository patientsRepository;

    @Autowired
    public PatientsServiceImpl(PatientsRepository patientsRepository) {
        this.patientsRepository = patientsRepository;
    }

    @Override
    public Page<Patient> search(SearchRequest request) {
        PageRequest pageRequest = PageRequest.of(request.getCurrentPageIndex(), request.getPageMaxIndex());
        String filter = request.getFilter();
        if (filter != null) {
            return patientsRepository.findAllByFirstNameContainingAndDeletedIsFalse(filter, pageRequest);
        }
        return patientsRepository.findAllByDeletedIsFalse(pageRequest);
    }

    @Override
    public void create(Patient patient) {
        patient.getStudyList().forEach(study -> study.setPatient(patient));
        patientsRepository.save(patient);
    }

    @Override
    public Patient findById(String id) throws PatientNotFoundException {
        Patient patient = patientsRepository.findById(id).orElse(null);
        if (patient == null) {
            throw new PatientNotFoundException("Patient not found !");
        }
        return patient;
    }

    @Override
    public Page<Patient> findAll(SearchRequest request) {
        PageRequest pageRequest = PageRequest.of(request.getCurrentPageIndex(), request.getPageMaxIndex());
        return patientsRepository.findAllByDeletedIsFalse(pageRequest);
    }

    @Override
    public void delete(String id) {
        try {
            Patient patient = findById(id);
            patient.setDeleted(true);
            patientsRepository.save(patient);
        } catch (PatientNotFoundException e) {
            e.printStackTrace();
        }
    }
}
