package com.caresyntax.challenge.service.impl;

import com.caresyntax.challenge.dataaccessobject.DoctorsRepository;
import com.caresyntax.challenge.domainObjects.model.Doctor;
import com.caresyntax.challenge.domainObjects.request.SearchRequest;
import com.caresyntax.challenge.service.DoctorsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DoctorsServiceImpl implements DoctorsService {

    @Autowired
    private DoctorsRepository doctorsRepository;

    @Override
    public Page<Doctor> findAll(SearchRequest request) {
        PageRequest pageRequest = PageRequest.of(request.getCurrentPageIndex(), request.getPageMaxIndex());
        return doctorsRepository.findAll(pageRequest);
    }

    @Override
    public Page<Doctor> search(SearchRequest request) {
        String operand = request.getOperand();
        String value = request.getValue();
        List<Doctor> doctorList = new ArrayList<>();
        PageRequest pageRequest = PageRequest.of(request.getCurrentPageIndex(), request.getPageMaxIndex());
        switch (request.getCriteria()) {
            case "firstName":
                if (operand.equals("=")) {
                    doctorList = doctorsRepository.findAllByFirstNameContaining(value, pageRequest);
                } else if (operand.equals("!=")) {
                    doctorList = doctorsRepository.findAllByFirstNameNotContaining(value, pageRequest);
                }
                break;
            case "lastName":
                if (operand.equals("=")) {
                    doctorList = doctorsRepository.findAllByLastNameContaining(value, pageRequest);
                } else if (operand.equals("!=")) {
                    doctorList = doctorsRepository.findAllByLastNameNotContaining(value, pageRequest);
                }
                break;
            default:
                break;
        }
        return new PageImpl<>(doctorList);
    }
}
