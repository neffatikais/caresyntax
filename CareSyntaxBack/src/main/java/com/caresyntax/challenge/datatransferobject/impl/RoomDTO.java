package com.caresyntax.challenge.datatransferobject.impl;

import com.caresyntax.challenge.datatransferobject.DataTransferObject;

import javax.validation.constraints.NotNull;

/**
 * Room Data transfer object
 */
public class RoomDTO extends DataTransferObject {

    @NotNull(message = "Name can not be null!")
    private String name;

    @NotNull(message = "Capacity can not be null!")
    private Integer capacity;

    public RoomDTO(String id, String name, Integer capacity) {
        super(id);
        this.name = name;
        this.capacity = capacity;
    }

    /**
     * @return Room DTO builder
     */
    public static RoomDTOBuilder newBuilder() {
        return new RoomDTOBuilder();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    /**
     * DTO builder
     */
    public static class RoomDTOBuilder {
        private String id;
        private String name;
        private Integer capacity;

        public RoomDTOBuilder setId(String id) {
            this.id = id;
            return this;
        }

        public RoomDTOBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public RoomDTOBuilder setCapacity(Integer capacity) {
            this.capacity = capacity;
            return this;
        }

        /**
         * @return Room data tranfer object
         */
        public RoomDTO createRoomDTO() {
            return new RoomDTO(id, name, capacity);
        }
    }
}
