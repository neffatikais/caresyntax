package com.caresyntax.challenge.datatransferobject.impl;

import com.caresyntax.challenge.datatransferobject.DataTransferObject;
import com.caresyntax.challenge.domainObjects.model.Study;
import com.caresyntax.challenge.domainValue.Sex;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * Patirnt Data transfer object
 */
public class PatientDTO extends DataTransferObject {

    @NotNull(message = "First name can not be null!")
    private String firstName;

    @NotNull(message = "Last name can not be null!")
    private String lastName;

    @NotNull(message = "Sex can not be null!")
    private Sex sex;

    @NotNull(message = "BirthDate can not be null!")
    private ZonedDateTime birthDate;

    private List<Study> studyList;

    public PatientDTO() {
    }

    public PatientDTO(String id, String firstName, String lastName, Sex sex, ZonedDateTime birthDate, List<Study> studyList) {
        super(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.sex = sex;
        this.birthDate = birthDate;
        this.studyList = studyList;
    }

    /**
     * @return DTO builder
     */
    public static PatientDTOBuilder newBuilder() {
        return new PatientDTOBuilder();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public ZonedDateTime getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(ZonedDateTime birthDate) {
        this.birthDate = birthDate;
    }

    public List<Study> getStudyList() {
        return studyList;
    }

    public void setStudyList(List<Study> studyList) {
        this.studyList = studyList;
    }


    /**
     * Patient DTO builder
     */
    public static class PatientDTOBuilder {
        private String id;
        private String firstName;
        private String lastName;
        private Sex sex;
        private ZonedDateTime birthDate;
        private List<Study> studyList;

        public PatientDTOBuilder setId(String id) {
            this.id = id;
            return this;
        }

        public PatientDTOBuilder setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public PatientDTOBuilder setLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public PatientDTOBuilder setSex(Sex sex) {
            this.sex = sex;
            return this;
        }

        public PatientDTOBuilder setBirthDate(ZonedDateTime birthDate) {
            this.birthDate = birthDate;
            return this;
        }

        public PatientDTOBuilder setStudyList(List<Study> studyList) {
            this.studyList = studyList;
            return this;
        }

        /**
         * @return create patient DTO
         */
        public PatientDTO createPatientDTO() {
            return new PatientDTO(id, firstName, lastName, sex, birthDate, studyList);
        }
    }
}
