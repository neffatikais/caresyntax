package com.caresyntax.challenge.datatransferobject.impl;

import com.caresyntax.challenge.datatransferobject.DataTransferObject;

import javax.validation.constraints.NotNull;

/**
 * Doctor data transfer object
 */
public class DoctorDTO extends DataTransferObject {

    @NotNull(message = "FirstName can not be null!")
    private String firstName;

    @NotNull(message = "LastName can not be null!")
    private String lastName;

    @NotNull(message = "Speciality can not be null!")
    private String speciality;

    public DoctorDTO(String id, String firstName, String lastName, String speciality) {
        super(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.speciality = speciality;
    }


    /**
     * @return DTO builder
     */
    public static DoctorDTOBuilder newBuilder() {
        return new DoctorDTOBuilder();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    /**
     * DTO builder
     */
    public static class DoctorDTOBuilder {
        private String id;
        private String firstName;
        private String lastName;
        private String speciality;

        public DoctorDTOBuilder setId(String id) {
            this.id = id;
            return this;
        }

        public DoctorDTOBuilder setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public DoctorDTOBuilder setLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public DoctorDTOBuilder setSpeciality(String speciality) {
            this.speciality = speciality;
            return this;
        }

        /**
         * @return Doctor dTO
         */
        public DoctorDTO createDoctorDTO() {
            return new DoctorDTO(id, firstName, lastName, speciality);
        }
    }
}
