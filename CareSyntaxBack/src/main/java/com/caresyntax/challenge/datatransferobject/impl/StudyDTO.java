package com.caresyntax.challenge.datatransferobject.impl;

import com.caresyntax.challenge.datatransferobject.DataTransferObject;
import com.caresyntax.challenge.domainObjects.model.Doctor;
import com.caresyntax.challenge.domainObjects.model.Patient;
import com.caresyntax.challenge.domainObjects.model.Room;
import com.caresyntax.challenge.domainValue.StudyStatus;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * Study Data transfer object
 */
public class StudyDTO extends DataTransferObject {

    @NotNull(message = "patient can not be null!")
    private Patient patient;

    private List<Doctor> doctorList;

    @NotNull(message = "room can not be null!")
    private Room room;

    private String description;

    @NotNull(message = "studyStatus can not be null!")
    private StudyStatus studyStatus;

    @NotNull(message = "startTime can not be null!")
    private ZonedDateTime startTime;

    @NotNull(message = "endTime can not be null!")
    private ZonedDateTime endTime;

    public StudyDTO() {
    }

    public StudyDTO(String id, Patient patient, List<Doctor> doctorList, Room room, String description, StudyStatus studyStatus, ZonedDateTime startTime, ZonedDateTime endTime) {
        super(id);
        this.patient = patient;
        this.doctorList = doctorList;
        this.room = room;
        this.description = description;
        this.studyStatus = studyStatus;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    /**
     * @return DTO builder
     */
    public static StudyDTOBuilder newBuilder() {
        return new StudyDTOBuilder();
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public List<Doctor> getDoctorList() {
        return doctorList;
    }

    public void setDoctorList(List<Doctor> doctorList) {
        this.doctorList = doctorList;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public StudyStatus getStudyStatus() {
        return studyStatus;
    }

    public void setStudyStatus(StudyStatus studyStatus) {
        this.studyStatus = studyStatus;
    }

    public ZonedDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(ZonedDateTime startTime) {
        this.startTime = startTime;
    }

    public ZonedDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(ZonedDateTime endTime) {
        this.endTime = endTime;
    }

    /**
     * Study DTO builder
     */
    public static class StudyDTOBuilder {
        private String id;
        private Patient patient;
        private List<Doctor> doctorList;
        private Room room;
        private String description;
        private StudyStatus studyStatus;
        private ZonedDateTime startTime;
        private ZonedDateTime endTime;


        /**
         * @return Study data transfer object
         */
        public StudyDTO createStudyDTO() {
            return new StudyDTO(id, patient, doctorList, room, description, studyStatus, startTime, endTime);
        }

        public StudyDTOBuilder setId(String id) {
            this.id = id;
            return this;
        }

        public StudyDTOBuilder setPatient(Patient patient) {
            this.patient = patient;
            return this;
        }

        public StudyDTOBuilder setDoctorList(List<Doctor> doctorList) {
            this.doctorList = doctorList;
            return this;
        }

        public StudyDTOBuilder setRoom(Room room) {
            this.room = room;
            return this;
        }

        public StudyDTOBuilder setDescription(String description) {
            this.description = description;
            return this;
        }

        public StudyDTOBuilder setStudyStatus(StudyStatus studyStatus) {
            this.studyStatus = studyStatus;
            return this;
        }

        public StudyDTOBuilder setStartTime(ZonedDateTime startTime) {
            this.startTime = startTime;
            return this;
        }

        public StudyDTOBuilder setEndTime(ZonedDateTime endTime) {
            this.endTime = endTime;
            return this;
        }
    }
}
