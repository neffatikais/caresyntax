package com.caresyntax.challenge.datatransferobject;

import javax.validation.constraints.NotNull;

import static com.caresyntax.challenge.utils.ResourceUtils.generateUniqueId;

public abstract class DataTransferObject {

    /**
     * Id auto generated
     */
    @NotNull(message = "id can not be null!")
    private String id = generateUniqueId();

    public DataTransferObject() {
    }

    public DataTransferObject(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
