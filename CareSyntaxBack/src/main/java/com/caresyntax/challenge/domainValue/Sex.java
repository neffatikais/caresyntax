package com.caresyntax.challenge.domainValue;

public enum Sex {
    MALE,
    FEMALE,
    OTHER
}
