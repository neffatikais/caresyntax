package com.caresyntax.challenge.domainValue;

public enum StudyStatus {
    PLANNED,
    IN_PROGRESSING,
    FINISHED
}
