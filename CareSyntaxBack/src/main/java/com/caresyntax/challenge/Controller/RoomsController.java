package com.caresyntax.challenge.Controller;

import com.caresyntax.challenge.datatransferobject.impl.RoomDTO;
import com.caresyntax.challenge.domainObjects.request.SearchRequest;
import com.caresyntax.challenge.service.RoomsService;
import com.caresyntax.challenge.utils.mapperUtils.impl.RoomMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/rooms")
public class RoomsController {

    private final RoomMapper roomMapper;

    private final RoomsService roomsService;

    /**
     * @param roomMapper   : Mapper for DTO pattern
     * @param roomsService : for business logic
     */
    @Autowired
    public RoomsController(RoomMapper roomMapper, RoomsService roomsService) {
        this.roomMapper = roomMapper;
        this.roomsService = roomsService;
    }

    /**
     * @param request : generic search request
     * @return : room data transfer object
     */
    @PostMapping("/")
    public ResponseEntity<Page<RoomDTO>> findAll(@RequestBody final SearchRequest request) {
        Page<RoomDTO> patientDTOPage = roomMapper.makeDTOPage(roomsService.findAll(request));
        return new ResponseEntity<>(patientDTOPage, HttpStatus.OK);
    }

    /**
     * @param request : generic search request
     * @return : room data transfer object
     */
    @PostMapping("/search")
    public ResponseEntity<Page<RoomDTO>> search(@RequestBody final SearchRequest request) {
        Page<RoomDTO> roomDTOPage = roomMapper.makeDTOPage(roomsService.search(request));
        return new ResponseEntity<>(roomDTOPage, HttpStatus.OK);
    }
}
