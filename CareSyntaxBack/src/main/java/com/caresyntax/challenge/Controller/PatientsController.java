package com.caresyntax.challenge.Controller;

import com.caresyntax.challenge.datatransferobject.impl.PatientDTO;
import com.caresyntax.challenge.domainObjects.model.Patient;
import com.caresyntax.challenge.domainObjects.request.SearchRequest;
import com.caresyntax.challenge.exceptions.PatientNotFoundException;
import com.caresyntax.challenge.service.PatientsService;
import com.caresyntax.challenge.utils.mapperUtils.impl.PatientMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/patients")
public class PatientsController {

    private final PatientsService patientsService;

    private final PatientMapper patientMapper;

    /**
     * @param patientMapper   : mapper for DTO pattern
     * @param patientsService : for business logic
     */
    @Autowired
    public PatientsController(PatientMapper patientMapper, PatientsService patientsService) {
        this.patientMapper = patientMapper;
        this.patientsService = patientsService;
    }

    /**
     * @param request : generic search request
     * @return paged results
     */
    @PostMapping("/")
    public ResponseEntity<Page<PatientDTO>> findAll(@RequestBody final SearchRequest request) {
        Page<PatientDTO> patientDTOPage = patientMapper.makeDTOPage(patientsService.findAll(request));
        return new ResponseEntity<>(patientDTOPage, HttpStatus.OK);
    }

    /**
     * @param id : id of patient
     * @return : patient data transfer object
     * @throws PatientNotFoundException
     */
    @GetMapping("/{id}")
    public ResponseEntity<PatientDTO> findPatient(@PathVariable final String id) throws PatientNotFoundException {
        PatientDTO patientDTO = patientMapper.makeDTO(patientsService.findById(id));
        return new ResponseEntity<>(patientDTO, HttpStatus.OK);
    }

    /**
     * @param id : id of patient to delete
     * @return Response entity with void content
     */
    @GetMapping("/delete/{id}")
    public ResponseEntity<Void> deletePatient(@PathVariable final String id) {
        patientsService.delete(id);
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.OK);
    }

    /**
     * @param request : generic search request
     * @return paged results
     */
    @PostMapping("/search")
    public ResponseEntity<Page<PatientDTO>> search(@RequestBody final SearchRequest request) {
        Page<PatientDTO> patientDTOPage = patientMapper.makeDTOPage(patientsService.search(request));
        return new ResponseEntity<>(patientDTOPage, HttpStatus.OK);
    }

    /**
     * @param request : Patient data transfer object
     * @return Response entity with void content
     */
    @PostMapping("/createOrUpdate")
    public ResponseEntity<Void> create(@RequestBody final PatientDTO request) {
        Patient patient = patientMapper.makeDO(request);
        patientsService.create(patient);
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.CREATED);
    }
}
