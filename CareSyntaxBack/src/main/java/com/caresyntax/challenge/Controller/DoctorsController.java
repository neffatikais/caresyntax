package com.caresyntax.challenge.Controller;

import com.caresyntax.challenge.datatransferobject.impl.DoctorDTO;
import com.caresyntax.challenge.domainObjects.request.SearchRequest;
import com.caresyntax.challenge.service.DoctorsService;
import com.caresyntax.challenge.utils.mapperUtils.impl.DoctorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/doctors")
public class DoctorsController {

    private final DoctorMapper doctorMapper;

    private final DoctorsService doctorsService;

    /**
     * @param doctorMapper   for DTO pattern
     * @param doctorsService for business logic
     */
    @Autowired
    public DoctorsController(DoctorMapper doctorMapper, DoctorsService doctorsService) {
        this.doctorMapper = doctorMapper;
        this.doctorsService = doctorsService;
    }

    /**
     * @param request : generic search request
     * @return paged results
     */
    @PostMapping("/")
    public ResponseEntity<Page<DoctorDTO>> findAll(@RequestBody final SearchRequest request) {
        Page<DoctorDTO> doctorDTOPage = doctorMapper.makeDTOPage(doctorsService.findAll(request));
        return new ResponseEntity<>(doctorDTOPage, HttpStatus.OK);
    }

    /**
     * @param request : generic search request
     * @return paged results
     */
    @PostMapping("/search")
    public ResponseEntity<Page<DoctorDTO>> search(@RequestBody final SearchRequest request) {
        Page<DoctorDTO> doctorDTOList = doctorMapper.makeDTOPage(doctorsService.search(request));
        return new ResponseEntity<>(doctorDTOList, HttpStatus.OK);
    }
}