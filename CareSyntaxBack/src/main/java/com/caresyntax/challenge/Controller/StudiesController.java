package com.caresyntax.challenge.Controller;

import com.caresyntax.challenge.datatransferobject.impl.StudyDTO;
import com.caresyntax.challenge.domainObjects.model.Study;
import com.caresyntax.challenge.domainObjects.request.SearchRequest;
import com.caresyntax.challenge.exceptions.StudyNotFoundException;
import com.caresyntax.challenge.service.StudiesService;
import com.caresyntax.challenge.utils.mapperUtils.impl.StudyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/studies")
public class StudiesController {

    private final StudiesService studiesService;

    private final StudyMapper studyMapper;

    /**
     * @param studyMapper    : Mapper for DTO pattern
     * @param studiesService : for business logic
     */
    @Autowired
    public StudiesController(StudyMapper studyMapper, StudiesService studiesService) {
        this.studyMapper = studyMapper;
        this.studiesService = studiesService;
    }

    /**
     * @param request : generic search request
     * @return : Study data transfer object
     */
    @PostMapping("/")
    public ResponseEntity<Page<StudyDTO>> findAll(@RequestBody final SearchRequest request) {
        Page<StudyDTO> studyDTOS = studyMapper.makeDTOPage(studiesService.findAll(request));
        return new ResponseEntity<>(studyDTOS, HttpStatus.OK);
    }

    /**
     * @param id : id of Patient
     * @return : Study data transfer object
     * @throws StudyNotFoundException
     */
    @GetMapping("/{id}")
    public ResponseEntity<StudyDTO> findStudy(@PathVariable final String id) throws StudyNotFoundException {
        StudyDTO studyDTO = studyMapper.makeDTO(studiesService.findById(id));
        return new ResponseEntity<>(studyDTO, HttpStatus.OK);
    }

    /**
     * @param id : id of Patient to delete
     * @return : Response entity with void content
     */
    @GetMapping("/delete/{id}")
    public ResponseEntity<Void> deleteStudy(@PathVariable final String id) {
        studiesService.delete(id);
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.OK);
    }

    /**
     * @param request : generic search request
     * @return : study data transfer object
     */
    @PostMapping("/search")
    public ResponseEntity<Page<StudyDTO>> search(@RequestBody final SearchRequest request) {
        Page<StudyDTO> StudyDTOList = studyMapper.makeDTOPage(studiesService.search(request));
        return new ResponseEntity<>(StudyDTOList, HttpStatus.CREATED);
    }

    /**
     * @param request : Study data transfer object
     * @return Response entity with void content
     */
    @PostMapping("/createOrUpdate")
    public ResponseEntity<Void> create(@RequestBody final StudyDTO request) {
        Study study = studyMapper.makeDO(request);
        studiesService.saveOrUpdate(study);
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.CREATED);
    }
}
