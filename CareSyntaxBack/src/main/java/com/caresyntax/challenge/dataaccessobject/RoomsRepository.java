package com.caresyntax.challenge.dataaccessobject;

import com.caresyntax.challenge.domainObjects.model.Room;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Rooms repository
 */
@Repository
public interface RoomsRepository extends JpaRepository<Room, String> {
    List<Room> findAllByNameContaining(String value, Pageable pageable);

    List<Room> findAllByNameNotContaining(String value, Pageable pageable);
}
