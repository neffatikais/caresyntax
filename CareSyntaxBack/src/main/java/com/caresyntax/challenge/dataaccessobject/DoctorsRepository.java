package com.caresyntax.challenge.dataaccessobject;

import com.caresyntax.challenge.domainObjects.model.Doctor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Doctor repository
 */
public interface DoctorsRepository extends JpaRepository<Doctor, String> {
    List<Doctor> findAllByFirstNameContaining(String firstName, Pageable pageable);

    List<Doctor> findAllByFirstNameNotContaining(String value, Pageable pageable);

    List<Doctor> findAllByLastNameContaining(String firstName, Pageable pageable);

    List<Doctor> findAllByLastNameNotContaining(String value, Pageable pageable);

}
