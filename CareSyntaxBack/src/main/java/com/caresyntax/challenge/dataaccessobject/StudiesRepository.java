package com.caresyntax.challenge.dataaccessobject;

import com.caresyntax.challenge.domainObjects.model.Study;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.ZonedDateTime;

/**
 * Studies repository
 */
public interface StudiesRepository extends JpaRepository<Study, String> {
    Page<Study> findAllByPatientIdAndDeletedIsFalse(String id, Pageable pageable);

    Page<Study> findAllByPatientIdAndDeletedIsFalseAndStartTimeContaining(String id, ZonedDateTime zonedDateTime, Pageable pageable);

    Page<Study> findAllByDeletedIsFalse(Pageable pageable);
}
