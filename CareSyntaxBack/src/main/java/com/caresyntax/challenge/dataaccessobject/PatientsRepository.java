package com.caresyntax.challenge.dataaccessobject;

import com.caresyntax.challenge.domainObjects.model.Patient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Patient repository
 */
@Repository
public interface PatientsRepository extends JpaRepository<Patient, String> {
    Page<Patient> findAllByDeletedIsFalse(Pageable pageable);

    Page<Patient> findAllByFirstNameContainingAndDeletedIsFalse(String FirstName, Pageable pageable);
}
