package com.caresyntax.challenge.domainObjects.model;

import com.caresyntax.challenge.domainObjects.DomainObject;
import com.caresyntax.challenge.domainValue.Sex;
import com.caresyntax.challenge.utils.ResourceUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.List;

@Entity
@Table(name = "patient")
public class Patient extends DomainObject {

    @Id
    private String id = ResourceUtils.generateUniqueId();

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Enumerated(EnumType.STRING)
    private Sex sex;

    @Column(name = "birth_date")
    private ZonedDateTime birthDate;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "patient", cascade = {CascadeType.ALL})
    private List<Study> studyList;

    public Patient() {
    }

    public Patient(String id, String firstName, String lastName, Sex sex, ZonedDateTime birthDate, List<Study> studyList) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.sex = sex;
        this.birthDate = birthDate;
        this.studyList = studyList;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public ZonedDateTime getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(ZonedDateTime birthDate) {
        this.birthDate = birthDate;
    }

    public List<Study> getStudyList() {
        return studyList;
    }

    public void setStudyList(List<Study> studyList) {
        this.studyList = studyList;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
