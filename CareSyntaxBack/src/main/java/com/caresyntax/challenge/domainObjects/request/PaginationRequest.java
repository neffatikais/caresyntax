package com.caresyntax.challenge.domainObjects.request;

public class PaginationRequest {

    private Integer currentPageIndex;

    private Integer pageMaxIndex;

    public Integer getCurrentPageIndex() {
        return currentPageIndex;
    }

    public void setCurrentPageIndex(Integer currentPageIndex) {
        this.currentPageIndex = currentPageIndex;
    }

    public Integer getPageMaxIndex() {
        return pageMaxIndex;
    }

    public void setPageMaxIndex(Integer pageMaxIndex) {
        this.pageMaxIndex = pageMaxIndex;
    }
}
