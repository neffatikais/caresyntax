package com.caresyntax.challenge.domainObjects.model;

import com.caresyntax.challenge.domainObjects.DomainObject;
import com.caresyntax.challenge.domainValue.StudyStatus;
import com.caresyntax.challenge.utils.ResourceUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.util.List;

@Entity
@Table
public class Study extends DomainObject {

    @Id
    private String id = ResourceUtils.generateUniqueId();

    @NotNull
    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @NotNull
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(name = "doctor_study",
            joinColumns = {@JoinColumn(name = "study_id")},
            inverseJoinColumns = {@JoinColumn(name = "doctor_id")})
    private List<Doctor> doctorList;

    @NotNull
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "room_id")
    private Room room;

    @NotNull
    @Column(columnDefinition = "varchar(1000)")
    private String description;

    @NotNull
    @Column(name = "study_status")
    private StudyStatus studyStatus;

    @NonNull
    @Column(name = "start_time")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime startTime;

    @Column(name = "end_time")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime endTime;

    public Study() {
    }

    public Study(String id, Patient patient, List<Doctor> doctorList, Room room, String description, StudyStatus studyStatus, ZonedDateTime startTime, ZonedDateTime endTime) {
        this.id = id;
        this.patient = patient;
        this.doctorList = doctorList;
        this.room = room;
        this.description = description;
        this.studyStatus = studyStatus;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public StudyStatus getStudyStatus() {
        return studyStatus;
    }

    public void setStudyStatus(StudyStatus studyStatus) {
        this.studyStatus = studyStatus;
    }

    public ZonedDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(ZonedDateTime startTime) {
        this.startTime = startTime;
    }

    public ZonedDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(ZonedDateTime endTime) {
        this.endTime = endTime;
    }

    public List<Doctor> getDoctorList() {
        return doctorList;
    }

    public void setDoctorList(List<Doctor> doctorList) {
        this.doctorList = doctorList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
