package com.caresyntax.challenge.domainObjects.model;

import com.caresyntax.challenge.domainObjects.DomainObject;
import com.caresyntax.challenge.utils.ResourceUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
public class Room extends DomainObject {

    @Id
    private String id = ResourceUtils.generateUniqueId();

    @NonNull
    private String name;

    @NonNull
    private Integer capacity;

    @JsonIgnore
    @OneToMany(mappedBy = "room", fetch = FetchType.LAZY)
    private List<Study> studyList;

    public Room() {
    }

    public Room(String id, String name, Integer capacity) {
        this.id = id;
        this.name = name;
        this.capacity = capacity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Study> getStudyList() {
        return studyList;
    }

    public void setStudyList(List<Study> studyList) {
        this.studyList = studyList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }
}
